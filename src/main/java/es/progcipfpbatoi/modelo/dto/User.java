package es.progcipfpbatoi.modelo.dto;

public class User {
    private String userName;
    private String password;

    private boolean esAdmin;

    public User(String userName, String password) {
        this(userName, password, false);
    }

    public User(String userName, String password, boolean esAdmin) {
        this.userName = userName;
        this.password = password;
        this.esAdmin = esAdmin;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        String toString = userName;
        toString += esAdmin?"(admin)":"(normal)";
        return toString;
    }
}
