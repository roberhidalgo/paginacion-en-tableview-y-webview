package es.progcipfpbatoi.modelo.dao;

import es.progcipfpbatoi.exceptions.WrongParameterException;
import es.progcipfpbatoi.modelo.dto.Persona;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class InMemoryPersonaDAO implements PersonaDAOInterface{

    private ArrayList<Persona> personas;

    public InMemoryPersonaDAO() {
        this.personas = new ArrayList<>();
        init();
    }

    @Override
    public int countAll() {
        return this.personas.size();
    }

    @Override
    public List<Persona> findAll() {
        return personas;
    }

    @Override
    public List<Persona> findAll(int fromIndex, int toIndex) throws WrongParameterException {

        if (fromIndex < 0)
            throw new WrongParameterException("El índice del primer elemento a mostrar es incorrecto");
        if (toIndex < fromIndex)
            throw new WrongParameterException("El índice del último elemento a mostrar es incorrecto");

        return this.personas.subList(fromIndex, toIndex);
    }

    private void init() {
        this.personas.add(new Persona("Persona1", "666666666"));
        this.personas.add(new Persona("Persona2", "666777666"));
        this.personas.add(new Persona("Persona3", "666666666"));
        this.personas.add(new Persona("Persona4", "666777666"));
        this.personas.add(new Persona("Persona5", "666666666"));
        this.personas.add(new Persona("Persona6", "666777666"));
        this.personas.add(new Persona("Persona7", "666666666"));
        this.personas.add(new Persona("Persona8", "666777666"));
        this.personas.add(new Persona("Persona9", "666666666"));
        this.personas.add(new Persona("Persona10", "666777666"));
        this.personas.add(new Persona("Persona11", "666666666"));
        this.personas.add(new Persona("Persona12", "666777666"));
        this.personas.add(new Persona("Persona13", "666666666"));
        this.personas.add(new Persona("Persona14", "666777666"));
        this.personas.add(new Persona("Persona15", "666666666"));
        this.personas.add(new Persona("Persona16", "666777666"));
        this.personas.add(new Persona("Persona17", "666666666"));
        this.personas.add(new Persona("Persona18", "666777666"));
        this.personas.add(new Persona("Persona19", "666666666"));
        this.personas.add(new Persona("Persona20", "666777666"));
        this.personas.add(new Persona("Persona21", "666666666"));
        this.personas.add(new Persona("Persona22", "666777666"));
        this.personas.add(new Persona("Persona23", "666666666"));
        this.personas.add(new Persona("Persona24", "666777666"));
        this.personas.add(new Persona("Persona25", "666666666"));
        this.personas.add(new Persona("Persona26", "666777666"));
        this.personas.add(new Persona("Persona27", "666666666"));
        this.personas.add(new Persona("Persona28", "666777666"));
        this.personas.add(new Persona("Persona29", "666666666"));
        this.personas.add(new Persona("Persona30", "666777666"));
        this.personas.add(new Persona("Persona31", "666777666"));
    }
}
