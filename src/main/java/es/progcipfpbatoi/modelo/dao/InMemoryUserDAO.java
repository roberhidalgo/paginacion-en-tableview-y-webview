package es.progcipfpbatoi.modelo.dao;

import es.progcipfpbatoi.modelo.dto.User;

import java.util.ArrayList;

public class InMemoryUserDAO implements UserDAOInterface {

    private ArrayList<User> users;

    public InMemoryUserDAO() {
        this.users = new ArrayList<>();
        init();
    }

    private void init() {
        this.users.add(new User("roberto", "1234", true));
        this.users.add(new User("pepe", "4321"));
        this.users.add(new User("anonymous", ""));
    }

    public User findByCredentials(String userName, String password) {
        for (User user: users) {
            if (user.getUserName().equals(userName)
                    && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }
}
