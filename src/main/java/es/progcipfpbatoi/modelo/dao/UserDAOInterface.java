package es.progcipfpbatoi.modelo.dao;

import es.progcipfpbatoi.modelo.dto.User;

public interface UserDAOInterface {

    User findByCredentials(String userName, String password);
}
