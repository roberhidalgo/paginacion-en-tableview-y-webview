package es.progcipfpbatoi.modelo.dao;

import es.progcipfpbatoi.exceptions.WrongParameterException;
import es.progcipfpbatoi.modelo.dto.Persona;

import java.util.List;

public interface PersonaDAOInterface {

    int countAll();

    List<Persona> findAll();

    /**
     * Recupera todos los registros en un rango dado
     * @param fromIndex índice del primer registro a recuperar
     * @param toIndex índice del ultimo registro a recuperar
     * @return Un listado de los registros demandados
     * @throws WrongParameterException En caso de recibir un dato incorrecto como parámetro
     */
    List<Persona> findAll(int fromIndex, int toIndex) throws WrongParameterException;


}
