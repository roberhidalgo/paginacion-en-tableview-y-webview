package es.progcipfpbatoi.controlador;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.net.URL;
import java.util.EventObject;
import java.util.ResourceBundle;

public class VistaWebController implements Initializable {

    private static final String URL_VIDEO_YOUTUBE = "https://www.youtube.com/embed/_HDZODOx7Zw?autoplay=1";

    @FXML
    private WebView webviewYoutube;

    private Initializable parentController;

    public VistaWebController(Initializable parentController) {
        this.parentController = parentController;
    }

    @FXML
    private void handleButtonVolver(ActionEvent event) {
        Stage stage = (Stage) ((Node) ((EventObject) event).getSource()).getScene().getWindow();
        ChangeScene.change(stage, parentController, "/vista/principal.fxml");
        webviewYoutube.getEngine().load(null);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        webviewYoutube.getEngine().load(URL_VIDEO_YOUTUBE);

    }
}
