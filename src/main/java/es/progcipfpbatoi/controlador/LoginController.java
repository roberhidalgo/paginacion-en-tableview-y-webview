package es.progcipfpbatoi.controlador;

import es.progcipfpbatoi.modelo.dao.InMemoryPersonaDAO;
import es.progcipfpbatoi.modelo.dao.UserDAOInterface;
import es.progcipfpbatoi.modelo.dto.User;
import es.progcipfpbatoi.session.UserSession;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import org.w3c.dom.events.Event;

import java.net.URL;
import java.util.EventObject;
import java.util.ResourceBundle;

public class LoginController implements Initializable {


    @FXML
    private TextField userTextField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Button goButton;

    @FXML
    private Button skipButton;


    private UserDAOInterface userDAO;

    public LoginController(UserDAOInterface userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    @FXML
    private void handleGoButtonPressed(ActionEvent event) {

        String userName = userTextField.getText();
        String password = passwordField.getText();

        User user = this.userDAO.findByCredentials(userName, password);

        if (user != null) {
            UserSession.setUserSession(user);
            changeToPrincipal(event);
        } else {
            mostrarAlertError("Credenciales incorrectas");
        }
    }

    @FXML
    private void handleSkipButtonPressed(ActionEvent event) {
        mostrarAlertError("Acceso como anónimo");
        changeToPrincipal(event);
    }

    private void changeToPrincipal(EventObject event) {
        Stage stage = (Stage) ((Node) ((EventObject) event).getSource()).getScene().getWindow();
        InMemoryPersonaDAO personaDAO = new InMemoryPersonaDAO();
        PrincipalController controller = new PrincipalController(personaDAO);
        ChangeScene.change(stage, controller, "/vista/principal.fxml");
    }

    private void mostrarAlertError(String mensaje) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(null);
        alert.setTitle("Error");
        alert.setContentText(mensaje);
        alert.showAndWait();
    }


}
