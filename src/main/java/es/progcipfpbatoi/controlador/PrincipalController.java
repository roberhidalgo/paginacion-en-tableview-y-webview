package es.progcipfpbatoi.controlador;

import es.progcipfpbatoi.exceptions.WrongParameterException;
import es.progcipfpbatoi.modelo.dao.PersonaDAOInterface;
import es.progcipfpbatoi.modelo.dto.Persona;
import es.progcipfpbatoi.modelo.dto.User;
import es.progcipfpbatoi.session.UserSession;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class PrincipalController implements Initializable {

    private final static int INDICE_PAGINA_INICIAL = 0;
    private final static int FILAS_POR_PAGINA = 10;

    @FXML
    TableColumn nombreCol;
    @FXML
    TableColumn telefonoCol;
    @FXML
    TableColumn botonCol;

    @FXML
    Hyperlink hlAtras;

    @FXML
    Hyperlink hlSiguiente;

    @FXML
    private TableView<Persona> tableViewPersonas;

    @FXML
    private Label labelLoggedUser;

    private PersonaDAOInterface personaDAO;
    private int indicePaginaActual;
    private int numRegistrosTotales;

    public PrincipalController(PersonaDAOInterface personaDAO) {
        this.personaDAO = personaDAO;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        nombreCol.setCellValueFactory(new PropertyValueFactory("nombre"));
        telefonoCol.setCellValueFactory(new PropertyValueFactory("telefono"));
        botonCol.setCellFactory(param -> new ButtonsCellController(this));

        numRegistrosTotales = this.personaDAO.countAll();
        indicePaginaActual = INDICE_PAGINA_INICIAL;
        mostrarPaginaActual();

        hlAtras.setDisable(true);
        hlSiguiente.setDisable(false);

        User user = UserSession.getUser();
        if (user != null) {
            labelLoggedUser.setText(user.toString());
        } else {
            labelLoggedUser.setText("Sin login");
        }
    }

    @FXML
    private void handleLinkSiguiente() {
        avanzarPagina();
        if ((indicePaginaActual+1) * FILAS_POR_PAGINA >= numRegistrosTotales) {
            hlSiguiente.setDisable(true);
        }

        if (indicePaginaActual > 0) {
            hlAtras.setDisable(false);
        }
    }

    @FXML
    private void handleLinkAtras() {
        retrocederPagina();
        if ((indicePaginaActual+1) * FILAS_POR_PAGINA < numRegistrosTotales) {
            hlSiguiente.setDisable(false);
        }

        if (indicePaginaActual == 0) {
            hlAtras.setDisable(true);
        }
    }

    private void retrocederPagina() {
        indicePaginaActual--;
        mostrarPaginaActual();
    }

    private void avanzarPagina() {
        indicePaginaActual++;
        mostrarPaginaActual();
    }

    private void mostrarPaginaActual() {
        try {
            tableViewPersonas.setItems(FXCollections.observableArrayList(getData(indicePaginaActual)));
        }catch (WrongParameterException ex) {
            mostrarAlertError(ex.getMessage());
        }
    }

    private List<Persona> getData(int indicePagina) throws WrongParameterException{
        int fromIndex = indicePagina * FILAS_POR_PAGINA;
        int toIndex = Math.min(fromIndex + FILAS_POR_PAGINA, numRegistrosTotales);
        return this.personaDAO.findAll(fromIndex, toIndex);
    }

    private void mostrarAlertError(String mensaje) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(null);
        alert.setTitle("Error");
        alert.setContentText(mensaje);
        alert.showAndWait();
    }
}
