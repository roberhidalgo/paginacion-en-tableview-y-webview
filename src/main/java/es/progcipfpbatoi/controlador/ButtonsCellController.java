package es.progcipfpbatoi.controlador;

import es.progcipfpbatoi.modelo.dto.Persona;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.EventObject;

public class ButtonsCellController extends TableCell<Persona, Void> {
    @FXML
    private AnchorPane root;

    @FXML
    private Button boton;

    private Initializable parentController;

    public ButtonsCellController(Initializable parentController) {

        this.parentController = parentController;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/vista/buttons_cell.fxml"));
        loader.setController(this);

        try {
            loader.load();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void updateItem(Void item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setGraphic(null);
        } else {
            setGraphic(root);
        }
    }

    @FXML
    private void handleButton(ActionEvent event) {

        Persona personaAEditar = getTableView().getItems().get(getIndex());
        System.out.println("Botón de " + personaAEditar + " pulsado");
        Stage stage = (Stage) ((Node) ((EventObject) event).getSource()).getScene().getWindow();
        VistaWebController vistaWebController = new VistaWebController(this.parentController);
        ChangeScene.change(stage, vistaWebController, "/vista/vista_web.fxml");
    }

}
