package es.progcipfpbatoi.session;

import es.progcipfpbatoi.modelo.dto.User;

public final class UserSession {
    private static User instance;

    public static void setUserSession(User user) {
        if(instance == null) {
            instance = user;
        }
    }

    public static User getUser() {
        return instance;
    }

    public static void cleanUserSession() {
        instance = null;
    }
}
