package es.progcipfpbatoi;

import es.progcipfpbatoi.controlador.*;
import es.progcipfpbatoi.modelo.dao.InMemoryPersonaDAO;
import es.progcipfpbatoi.modelo.dao.InMemoryUserDAO;
import javafx.application.Application;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App extends Application
{
    @Override
    public void start(Stage stage) throws IOException {

        InMemoryUserDAO userDAO = new InMemoryUserDAO();
        LoginController controller = new LoginController(userDAO);
        ChangeScene.change(stage, controller, "/vista/login.fxml");
    }

    public static void main(String[] args) {
        launch();
    }
}
